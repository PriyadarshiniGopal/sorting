﻿using System;

namespace Sorting
{
    // Interface to declare methods for sorting application
    public interface ISort
    {
        // interface members
        void Sort(int length);
        void maxElement(int length);
        void minElement(int length);
        void midElement(int length);
    }
    // Class for bubble sorting
    public class BubbleSort : ISort
    {
        // property for array values
        private int[] arr { get; set; }

        /// <summary>
        /// Constructor for bubble sort
        /// </summary>
        /// <param name="array">Array contains all values</param>
        public BubbleSort(int[] array)
        {
            arr = array;
        }

        /// <summary>
        /// Method to sort the array using bubble sort
        /// </summary>
        /// <param name="length">number of elements in array</param>
        public void Sort(int length)
        {
            for(int i = 0; i < length - 1; i++)
            {
                for(int j = 0; j < length - i - 1; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
        }

        /// <summary>
        /// Method to get minimum element in array
        /// </summary>
        /// <param name="length">Number of elements in array</param>
        public void minElement(int length)
        {
            Console.WriteLine("Minimum Element  " + arr[0]);
        }

        /// <summary>
        /// Method to get maximum element in array
        /// </summary>
        /// <param name="length">Number of elements in array</param>
        public void maxElement(int length)
        {
            Console.WriteLine("Minimum Element  " + arr[length - 1]);
        }

        /// <summary>
        /// Method to get middle element in array
        /// </summary>
        /// <param name="length">Number of elements in array</param>
        public void midElement(int length)
        {
            if (length % 2 == 0)
            {
                Console.WriteLine("Middle Elements  " + arr[length / 2] + " , " + arr[length / 2 - 1]);
            }
            else
                Console.WriteLine("Middle Elements  " + arr[length / 2]);
        }
    }
    // Class for Selection sorting
    public class SelectionSort : ISort
    {
        // property for array values
        private int[] arr { get; set; }

        /// <summary>
        /// Constructor for bubble sort
        /// </summary>
        /// <param name="array">Array contains all values</param>
        public SelectionSort(int[] array)
        {
            arr = array;
        }

        /// <summary>
        /// Method to sort the array using bubble sort
        /// </summary>
        /// <param name="length">number of elements in array</param>
        public void Sort(int length)
        {
            for (int i = 0; i < length - 1; i++)
            {
                int min_index = i;
                for (int j = i + 1; j < length; j++)
                {
                    if (arr[min_index] > arr[j])
                    {
                        min_index = j;
                    }
                }
                int temp = arr[i];
                arr[i] = arr[min_index];
                arr[min_index] = temp;
            }
        }

        /// <summary>
        /// Method to get minimum element in array
        /// </summary>
        /// <param name="length">Number of elements in array</param>
        public void minElement(int length)
        {
            Console.WriteLine("Minimum Element  " + arr[0]);
        }

        /// <summary>
        /// Method to get maximum element in array
        /// </summary>
        /// <param name="length">Number of elements in array</param>
        public void maxElement(int length)
        {
            Console.WriteLine("Minimum Element  " + arr[length - 1]);
        }

        /// <summary>
        /// Method to get middle element in array
        /// </summary>
        /// <param name="length">Number of elements in array</param>
        public void midElement(int length)
        {
            if (length % 2 == 0)
            {
                Console.WriteLine("Middle Elements  " + arr[length / 2] + " , " + arr[length / 2 - 1]);
            }
            else
                Console.WriteLine("Middle Elements  " + arr[length / 2]);
        }
    }
    class Sort
    {
        /// <summary>
        /// Method to display Array values and Minimum,maximum and middle
        /// </summary>
        /// <param name="n">Array length</param>
        /// <param name="array">array values</param>
        /// <param name="sort">instance of a class</param>
        static void display(int n,int[] array,ISort sort)
        {
            for (int i = 0; i < n; i++)
                Console.Write(array[i] + " ");
            Console.WriteLine();
            sort.minElement(n);
            sort.maxElement(n);
            sort.midElement(n);
        }
        static void Main(string[] args)
        {
            Console.Write("Enter Number of Elements  ");
            int n = Convert.ToInt32(Console.ReadLine());
            int[] array = new int[n];
            Console.Write("Enter elements one by one  ");
            for(int i = 0; i < n; i++)
            {
                array[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("Press 1.Bubble sort   Press 2.Selection sort");
            int opt = Convert.ToInt32(Console.ReadLine());
            if (opt == 1)
            {
                BubbleSort sort = new BubbleSort(array);
                sort.Sort(n);
                Console.Write("Elements After Bubble sort ");
                Sort.display(n, array, sort);
            }
            else
            {
                SelectionSort sort = new SelectionSort(array);
                sort.Sort(n);
                Console.Write("Elements After Selection sort ");
                Sort.display(n, array, sort);
            }
        }
    }
}
